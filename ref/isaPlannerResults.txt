----------------------------------------------------------------------------------
-- Induction and Rippling with Case-Analysis and Lemma Calculation --
----------------------------------------------------------------------------------

47 theorems solved by rippling using only basic definitions.

                                           term                                            | time
-------------------------------------------------------------------------------------------+-------
 take n xs @ drop n xs = xs                                                                | 0.284
 count n l + count n m = count n (l @ m)                                                   | 0.131
 count n l leq count n (l @ m)                                                             |   0.2
 suc 0 + count n l = count n (n # l)                                                       | 0.086
 n = x ==> suc 0 + count n l = count n (x # l)                                             | 0.132
 n minus (n + m) = 0                                                                       | 0.174
 (n + m) minus n = m                                                                       | 0.177
 (k + m) minus (k + n) = m minus n                                                         | 0.079
 (i minus j) minus k = i minus (j + k)                                                     |  0.27
 m minus m = 0                                                                             | 0.068
 drop 0 xs = xs                                                                            | 0.068
 drop n (map f xs) = map f (drop n xs)                                                     |   0.2
 drop (suc n) (x # xs) = drop n xs                                                         | 0.073
 filter P (xs @ ys) = filter P xs @ filter P ys                                            | 0.069
 len ins x l = suc len l                                                                   | 0.035
 xs = [] ==> last (x # xs) = x                                                             | 0.012
 n leq 0 = (n = 0)                                                                         | 0.065
 i less (suc (i + m))                                                                      | 0.251
 len drop n xs = (len xs) minus n                                                          |  0.36
 len sort l = len l                                                                        | 0.047
 n leq (n + m)                                                                             | 0.168
 CaseAnalysis2.max (CaseAnalysis2.max a b) c =
CaseAnalysis2.max a (CaseAnalysis2.max b c)                                                | 0.632
 CaseAnalysis2.max a b = CaseAnalysis2.max b a                                             | 0.254
 (CaseAnalysis2.max a b = a) = b leq a                                                     | 0.262
 (CaseAnalysis2.max a b = b) = a leq b                                                     |  0.27
 x mem l --> x mem l @ t                                                                   | 0.069
 x mem t --> x mem l @ t                                                                   | 0.025
 x mem l @ [x]                                                                             | 0.023
 x mem ins_1 x l                                                                           | 0.027
 x mem ins x l                                                                             | 0.029
 CaseAnalysis2.min (CaseAnalysis2.min a b) c =
CaseAnalysis2.min a (CaseAnalysis2.min b c)                                                | 0.646
 CaseAnalysis2.min a b = CaseAnalysis2.min b a                                             | 0.259
 (CaseAnalysis2.min a b = a) = a leq b                                                     | 0.265
 (CaseAnalysis2.min a b = b) = b leq a                                                     | 0.261
 dropWhile (%x. False) xs = xs                                                             | 0.011
 takeWhile (%x. True) xs = xs                                                              | 0.009
 ~ x mem delete x l                                                                        | 0.028
 count n (x @ [n]) = suc count n x                                                         | 0.064
 count n [h] + count n t = count n (h # t)                                                 | 0.144
 take 0 xs = []                                                                            | 0.068
 take n (map f xs) = map f (take n xs)                                                     | 0.218
 take (suc n) (x # xs) = x # take n xs                                                     | 0.072
 takeWhile P xs @ dropWhile P xs = xs                                                      |  0.05
 zip (x # xs) ys = (case ys of [] => [] | y # ys => (x, y) # zip xs ys)                    | 0.222
 zip (x # xs) (y # ys) = (x, y) # zip xs ys                                                | 0.109
 zip [] ys = []                                                                            | 0.078
 height (mirror a) = height a                                                              | 0.282
(47 rows)


40 theorems not solved by rippling only from basic definitions,
with a timeout limit of 30 seconds.

                                      term                                      | time
--------------------------------------------------------------------------------+-------
 xs ~= [] ==> butlast xs @ [last xs] = xs                                       | 0.005
 butlast (xs @ ys) = (if ys = [] then butlast xs else xs @ butlast ys)          | 0.268
 butlast xs = take ((len xs) minus (suc 0)) xs                                  |  2.41
 butlast (xs @ [x]) = xs                                                        | 0.026
 count n l = count n (rev l)                                                    | 0.075
 count x l = count x (sort l)                                                   | 0.075
 (m + n) minus n = m                                                            | 0.184
 (i minus j) minus k = i minus (k minus j)                                      | 0.459
 drop n (xs @ ys) = drop n xs @ drop (n minus (len xs)) ys                      | 1.968
 drop n (drop m xs) = drop (n + m) xs                                           | 0.294
 drop n (take m xs) = take (m minus n) (drop n xs)                              | 0.761
 drop n (zip xs ys) = zip (drop n xs) (drop n ys)                               |  0.86
 ys = [] ==> last (xs @ ys) = last xs                                           |  0.07
 ys ~= [] ==> last (xs @ ys) = last ys                                          | 0.038
 last (xs @ ys) = (if ys = [] then last xs else last ys)                        | 0.249
 xs ~= [] ==> last (x # xs) = last xs                                           | 0.008
 n less (len xs) ==> last (drop n xs) = last xs                                 | 0.147
 last (xs @ [x]) = x                                                            | 0.029
 i less (suc (m + i))                                                           |  0.49
 (len filter P xs) leq (len xs)                                                 | 0.336
 len butlast xs = (len xs) minus (suc 0)                                        | 0.756
 (len delete x l) leq (len l)                                                   | 0.352
 n leq (m + n)                                                                  | 0.174
 m leq n ==> m leq (suc n)                                                      | 0.071
 x less y ==> x mem ins y l = x mem l                                           |  0.03
 x ~= y ==> x mem ins y l = x mem l                                             | 0.084
 rev (drop i xs) = take ((len xs) minus i) (rev xs)                             | 0.783
 rev (filter P xs) = filter P (rev xs)                                          | 0.096
 rev (take i xs) = drop ((len xs) minus i) (rev xs)                             | 0.777
 n ~= h ==> count n (x @ [h]) = count n x                                       | 0.216
 count n t + count n [h] = count n (h # t)                                      | 1.827
 sorted l ==> sorted (insort x l)                                               | 0.023
 sorted (sort l)                                                                | 0.005
 ((suc m) minus n) minus (suc k) = (m minus n) minus k                          | 0.464
 take n (xs @ ys) = take n xs @ take (n minus (len xs)) ys                      | 1.986
 take n (drop m xs) = drop m (take (n + m) xs)                                  | 0.691
 take n (zip xs ys) = zip (take n xs) (take n ys)                               | 0.726
 zip (xs @ ys) zs = zip xs (take (len xs) zs) @ zip ys (drop (len xs) zs)       |  0.73
 zip xs (ys @ zs) =
zip (take (length ys) xs) ys @ zip (drop (length ys) xs) zs                     | 9.439
 len xs = len ys ==> zip (rev xs) (rev ys) = rev (zip xs ys)                    | 0.427
(40 rows)
