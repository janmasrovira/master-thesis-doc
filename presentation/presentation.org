#+latex_compiler: xelatex
#+LATEX_CLASS_OPTIONS: [table, x11names]
#+OPTIONS: H:2 toc:nil

#+LATEX_HEADER: \usepackage{unicode-math}
#+LaTeX_HEADER: \usepackage{booktabs}

#+LATEX_HEADER: \setmonofont{FreeMono}

#+latex_header: \usetheme{Rochester}
#+latex_header: \usecolortheme{crane}
#+LATEX_HEADER: \newcommand{\yes}{\cellcolor{green!50}}
#+LATEX_HEADER: \newcommand{\no}{\cellcolor{yellow!60}}

#+AUTHOR: Jan Mas Rovira\newline Supervised by Albert Rubio Gimeno
#+DATE: June 2018

#+begin_export LaTeX
\title{Automatic inductive equational reasoning}
\subtitle{Phileas}
\titlegraphic{\includegraphics[width=6cm]{images/cs}}
\maketitle

#+end_export


* Introduction
** Who is Phileas?
*** Col left                                                          :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :END:

    #+ATTR_LATEX: :float t :width 0.75\textwidth :placement [H]
    [[file:images/balloon.jpg]]

*** Col right                                                         :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.50
    :END:
    Phileas is an explorer...
    #+BEAMER: \pause
    \newline
    of *proofs*.
   #+begin_export LaTeX
   \\~\\
   #+end_export
** Motivation
   Why proving properties about programs?
   - Ensure *correctness*.
   - Proofs are *absolute*. Tests are not.

   #+Begin_export LateX
   ~\\
   #+end_export
*** Col left                                                          :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :END:


    On 4 June 1996, the Ariane 5 launch ended in a
    failure due to an overflow error.

*** Col right                                                         :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.50
    :END:
    #+ATTR_LATEX: :float t :width 0.85\textwidth :placement [H]
    [[file:images/explosion.jpg]]
** What kind of proofs?
   *Inductive equational* proofs on *functional* terms.
   #+begin_export LaTeX
   \\~\\
   #+end_export

   Example properties:

   - \texttt{member x (insert x l) ≡ True}
   - \texttt{length l ≡ length (map f l)}
   - \texttt{map id ≡ id}

** Functional is the way
   Why functional?
   - Our use case involves a functional language (core of ABS).
   - No side effects $→$ We can use *equational reasoning*.
   #+Begin_export LateX
   ~\\
   #+end_export

*** Equational reasoning
   if $a = b$ then we can replace any $a$ by a $b$ and vice versa.

** Our use case
   We want to use Phileas in the context of *concurrent system verification*.

   A concurrent system is composed of several process that execute
   simultaneously and access shared resources.
   #+Begin_export LateX
   ~\\
   #+end_export

   Interesting to verify properties such as:
   - No deadlocks. A no-exit block will not happen.
   - No starvation. A process will not be denied a resource forever.
   - Liveliness. An event will eventually happen.
   - ...
** How to verify?

   Concurrent systems can be verified by exploring all execution paths.

    #+ATTR_LATEX: :float t :width 0.75\textwidth :placement [H]
    [[file:images/paths.jpg]]
   *Too many paths!* Can we prune the search space?
** CDPOR
*** Optimization
    If process $A$ and $B$ do not interact—they are independent—there is no need
    to check both $A∣B$ and $B∣A$.
*** End of block                                               :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    /Constrained dynamic partial order reduction/ (*CDPOR*) is a technique that prunes
    paths by using sufficient conditions for two processes to be independent.


   #+Begin_export LateX
   ~\\
   #+end_export
    Most of these conditions are *equalities* between *functional terms*.
   #+Begin_export LateX
   ~\\
   #+end_export

** CDPOR

   We can use Phileas to determine which of these equalities will always be true.
   #+Begin_export LateX
   ~\\
   #+end_export

   If an equality is *proved true* for all input possible variable values, it
   can be *removed*.

   #+Begin_export LateX
   ~\\
   #+end_export

   Removing a condition may allow further pruning of the search
   space, achieving *exponential reductions* over existing implementations

* Inductive equational reasoning
** \texttt{ }
    \begin{center} {\Large
    The language Phi
    } \end{center}
** Phi
   Phi is Phileas internal language.

   #+Begin_export LateX
   ~\\
   #+end_export
   An expression can be:
   - A variable.
   - A constructor.
   - An application.
   - A case expression.
   - A lambda abstraction.
   - A let binding.

    #+Begin_export LateX
   ~\\
   #+end_export

   Phi only supports *algebraic data types*.

** \texttt{ }
    \begin{center} {\Large
    Algebraic data types and structural induction
    } \end{center}
** Algebraic data types
   - Phileas only supports *algebraic data types*.

     =data Nat = Zero | Suc Nat=

     =data List a = Nil | Cons a (List a)=
   #+begin_export LaTeX
   ~\\
   #+end_export

   - Values can only be built by applying *constructors*.
     \newline The list of naturals =[1,2]= can be expressed as.

     $$\texttt{Cons (Suc Zero) (Cons (Suc (Suc Zero)) Nil)}$$

   - Constructors are the key to *structural induction*.
** Structural induction - Example
   We want to prove that

   #+begin_export LaTeX
   ~\\
   #+end_export

   =length (a ++ b) ≡ length a + length b=

** Example: Definitions
  #+BEGIN_SRC text
  data Nat = Zero | Suc Nat
  data List = Nil | Cons Nat List

  (+) ∷ Nat → Nat → Nat
  Zero + b = b
  Suc a + b = Suc (a + b)

  (++) ∷ List → List → List
  Nil ++ b = b
  Cons a as ++ b = Cons a (as ++ b)

  #+END_SRC
** Example: Proof - Induction
  :PROPERTIES:
  :BEAMER_opt: fragile
  :END:

    #+begin_export LaTeX
    \begin{columns}
    \begin{column}{0.40\columnwidth}
    \begin{minted}[,fontsize=\tiny]{text}
    data Nat = Zero | Suc Nat
    data List = Nil | Cons Nat List

    \end{minted}
    \end{column}

    \begin{column}{0.45\columnwidth}
    \begin{minted}[,fontsize=\tiny]{text}
    (+) ∷ Nat → Nat → Nat
    Zero + b = b
    Suc a + b = Suc (a + b)

    (++) ∷ List → List → List
    Nil ++ b = b
    Cons a as ++ b = Cons a (as ++ b)
    \end{minted}
    \end{column}
    \end{columns}
    ~\\
    \hrule
    ~\\
   #+end_export
    Goal:
   $$\texttt{length (a ++ b) ≡ length a + length b}$$

   We will apply induction on \texttt{a}.
   - Base case: \texttt{a ≡ Nil}
   - Inductive case: \texttt{a ≡ Cons x xs}

** Example: Proof - Base case
  :PROPERTIES:
  :BEAMER_opt: fragile
  :END:

    #+begin_export LaTeX
    \begin{columns}
    \begin{column}{0.40\columnwidth}
    \begin{minted}[,fontsize=\tiny]{text}
    data Nat = Zero | Suc Nat
    data List = Nil | Cons Nat List

    \end{minted}
    \end{column}

    \begin{column}{0.45\columnwidth}
    \begin{minted}[,fontsize=\tiny]{text}
    (+) ∷ Nat → Nat → Nat
    Zero + b = b
    Suc a + b = Suc (a + b)

    (++) ∷ List → List → List
    Nil ++ b = b
    Cons a as ++ b = Cons a (as ++ b)
    \end{minted}
    \end{column}
    \end{columns}
    ~\\
    \hrule
    ~\\
    {\tiny
    Prove $$\texttt{length\ (Nil \texttt{++} b) ≡ length\ Nil + length\ b}$$

    \begin{flalign*}
    \texttt{length\ (Nil \texttt{++} b)} &≡ \texttt{length\ Nil + length\ b}
    &\text{Definition of \texttt{++}} \\
    \texttt{length\ b} &≡ \texttt{length\ Nil + length\ b}
    &\text{Definition of \texttt{length}} \\
    \texttt{length\ b} &≡ \texttt{Zero + length\ b}
    &\text{Definition of \texttt{+}} \\
    \texttt{length\ b} &≡ \texttt{length\ b}
    &\text{Reflexivity}\\
    \end{flalign*}
    }%
        #+end_export

** Example: Proof - Inductive case
  :PROPERTIES:
  :BEAMER_opt: fragile
  :END:

    #+begin_export LaTeX
    \begin{columns}
    \begin{column}{0.40\columnwidth}
    \begin{minted}[,fontsize=\tiny]{text}
    data Nat = Zero | Suc Nat
    data List = Nil | Cons Nat List

    \end{minted}
    \end{column}

    \begin{column}{0.45\columnwidth}
    \begin{minted}[,fontsize=\tiny]{text}
    (+) ∷ Nat → Nat → Nat
    Zero + b = b
    Suc a + b = Suc (a + b)

    (++) ∷ List → List → List
    Nil ++ b = b
    Cons a as ++ b = Cons a (as ++ b)
    \end{minted}
    \end{column}
    \end{columns}
    ~\\
    \hrule
    ~\\
    {\tiny
    Assuming $$\texttt{length\ (xs \texttt{++} b) ≡ length\ xs + length\ b}$$

    Prove $$\texttt{length\ ((Cons\ x\ xs) \texttt{++} b) ≡ length\ (Cons\ x\ xs) + length\ b}$$

    \begin{flalign*}
    \texttt{length\ ((Cons\ x\ xs) ++ b)} &≡ \texttt{length\ (Cons\ x\ xs) + length\ b}
    &\text{Definition of \texttt{++}} \\
    \texttt{length\ (Cons\ x\ (xs ++ b)} &≡ \texttt{length\ (Cons\ x\ xs) + length\ b}
    &\text{Definition of \texttt{length}} \\
    \texttt{Suc\ (length\ (xs ++ b))} &≡ \texttt{Suc\ (length\ xs) + length\ b}
    &\text{Definition of \texttt{+}} \\
    \texttt{Suc\ (length\ (xs ++ b))} &≡ \texttt{Suc\ (length\ xs + length\ b)}
    &\text{Congruence on \texttt{Suc}} \\
    \texttt{length\ (xs ++ b)} &≡ \texttt{length\ xs + length\ b}
    &\text{Close using induction hypothesis} \\
    &&∎ \\
    \end{flalign*}
    }%
        #+end_export


* Search algorithm
** \texttt{ }
    \begin{center} {\Large
    Search algorithm
    } \end{center}

** The algorithm
   Phileas may prove properties of the form

   $$∀ a_1,…,a_n.\ e_1≡e_2 ⇒ … ⇒ e_{m-1} ≡ e_m$$

   #+begin_export LaTeX
   ~\\
   #+end_export

   Phileas tries to find a valid *proof* for a property by doing a backtracking
   search with pruning.

   #+begin_export LaTeX
   ~\\
   #+end_export

   A proof is a tree where nodes are *proof steps*.

** Main proof steps
   Main proof steps:
   - _Reflexivity_. Proves =a ≡ a=.
   - _Congruence_. E.g., proves =Pair a b ≡ Pair a' b'=, if it can prove
     =a ≡ a'= and =b ≡ b'=.
   - _Absurd in hypotheses_. E.g., proves =Nil ≡ Cons x xs ⇒ P=
   - _Structural induction_ on an expression =e=. E.g., if =e= has type =Nat=,
     it must prove the property for =e ≡ Zero=\newline and =e ≡ Suc n=.

** Simplifications
   Simplifications are applied before each proof step.

   #+begin_export LaTeX
   ~\\
   #+end_export

   They are *not explicit* in the final proof. They should be obvious to the
   human eye.

   #+begin_export LaTeX
   ~\\
   #+end_export

   The most important simplifications are similar to evaluation rules.
** Main simplification rules
   - _Use definition_. Replaces a defined variable by its definition. Cannot be
     applied everywhere.
   - _Beta reduction_. E.g., rewrites =(λx. Suc x) a= into =Suc a=.
   - _Pattern match_. E.g., rewrites

    #+ATTR_LaTeX: :options fontsize=\footnotesize
    #+BEGIN_SRC text
    case Cons a as of
      Nil → e
      Cons x xs → x
    #+END_SRC
    into =a=.

** Example Phileas proof
    The proof of
    $$\texttt{length (a ++ b) ≡ length a + length b}$$

    #+begin_export LaTeX
    ~\\
    #+end_export

    generated by Phileas is:
     #+ATTR_LaTeX: :options fontsize=\small
    #+BEGIN_SRC text
 Induction on (a ∷ List)
   Nil
     Reflexivity ... ≡ ...
   Cons
     Congruence on Suc
         Close using hypothesis ... ≡ ...
    #+END_SRC
** \texttt{ }
    \begin{center} {\Large
    Phileas pipeline
    } \end{center}
** Pipeline
     #+ATTR_LATEX: :width \textwidth
     [[file:images/compile.pdf]]

* Phileas pipeline

* Zeno and IsaPlanner
* Results
** \texttt{ }
    \begin{center} {\Large
    Results
    } \end{center}
** Zeno and IsaPlanner
   We have compared Phileas to two other automatic inductive theorem provers:
   - *IsaPlanner*. Developed at Edinburgh University. It is a plugin for the
     Isabelle proof assistant.
   - *Zeno*. Developed at Imperial College London. It is a prover with a similar
     pipeline to Phileas.
** About Zeno
   Zeno's highlights.

   Positive:
   - Generates proofs for Isabelle.
   - Can find counterexamples for some false properties.

   Negative:
   - Unmaintained. It is difficult to compile it and does not work with up to date
     versions of GHC.
   - Does not fully support type polymorphism.
   - The counterexample finder is flawed and sometimes generates incorrect
     counterexamples.

** Test suites
   Phileas has been tested on 3 test suites:
   - Zeno's test suite (41 properties).
   - IsaPlanner's test suite (86 properties).
   - Our test suite (72 properties).
** Zeno's test suite
   This test suite is composed of 41 example properties in Zeno's package.
   - 12 about natural numbers.
   - 27 about lists of natural numbers.
   - 2 about binary trees.

   #+ATTR_LaTeX: :booktabs t :placement [H] :align l|cr|cr|cr
  | Prover  | Naturals |       | Lists |       | Trees |      |
  |---------+----------+-------+-------+-------+-------+------|
  | Zeno    |       12 |  100% |    27 |  100% |     0 |   0% |
  | Phileas |        7 | 58.3% |    20 | 74.1% |     2 | 100% |

** IsaPlanner test suite
   This test suite is composed of 86 properties.
   - 14 about natural numbers.
   - 71 about lists of natural numbers.
   - 1 about binary trees.

   #+ATTR_LaTeX: :booktabs t :placement [H] :align l|cr|cr|cr
  | Prover     | Naturals |       | Lists |       | Trees |      |
  |------------+----------+-------+-------+-------+-------+------|
  | Zeno       |       13 | 92.9% |    61 | 85.9% |     1 | 100% |
  | IsaPlanner |        8 | 57.1% |    40 | 56.3% |     1 | 100% |
  | Phileas    |        8 | 57.1% |    58 | 81.2% |     1 | 100% |

** Phileas test suite: Type classes
   *Type classes* are used to provide a common *interface* to types that have a
   similar characteristic.
   #+begin_export LaTeX
   ~\\
   #+end_export

   Example:
  #+ATTR_LaTeX: :options fontsize=\small
   #+BEGIN_SRC text
   class Semigroup g where
     (<>) ∷ g a → g a → g a

   instance Semigroup (List a) where
     (<>) = (++)
   #+END_SRC

   An instance of a semigroup should satisfy one law that is not checked by the
   compiler:
   - =(a <> b) <> c ≡ a <> (b <> c)=
** Type class graph
   Haskell has many type classes in the =base= package. We picked some of the
   most common:

  #+ATTR_LATEX: :float t :width \textwidth :placement [H]
  [[file:images/classes.pdf]]
** Type class properties
   Phileas and Zeno were tested on a total of 74 properties.

   #+begin_export LaTeX
   ~\\
   #+end_export

   Zeno *failed 70 tests*:
   - 2 were incorrect counterexamples.
   - 68 were crashes (type errors).

   #+begin_export LaTeX
   ~\\
   #+end_export

   Phileas *only failed 4 tests*.
   - 4 gentle fails.

** Type class properties
   Phileas results in detail:

   #+Begin_export LateX
\begin{center}
\resizebox{\columnwidth}{!}{%
\begin{tabular}{l|ccccccc}
\toprule
 & Functor & Applicative & Monad & Alternative & MonadPlus & Semigroup & Monoid\\
\midrule
List & \yes & \no 1 & \no 1 & \yes & \yes & \yes & \yes\\
Maybe & \yes & \yes & \yes & \yes & \yes &  & \\
Either & \yes & \yes & \yes &  &  & \yes & \\
Binary Tree & \yes &  &  &  &  &  & \\
Reader & \yes & \yes & \yes &  &  &  & \\
State & \yes & \no 1 & \yes &  &  &  & \\
Pair & \yes &  &  &  &  & \yes & \yes\\
Endofunction &  &  &  &  &  & \yes & \yes\\
Naturals (\(+\)) &  &  &  &  &  & \yes & \yes\\
Naturals (\(*\)) &  &  &  &  &  & \no 1 & \yes\\
\bottomrule
\end{tabular}
}
\end{center}
   #+end_export


* Demonstration
** Demonstration
   [[https://www.youtube.com/watch?v=hzFzKS_6qpQ]]
* Conclusions
** \texttt{ }
    \begin{center} {\Large
    Conclusions
    } \end{center}
** Conclusions
   _Challenges_:
   - Phileas is a complex system. Implementing it and debugging it is not easy.
   _Achievements_:
   - Unlike similar tools, Phileas truly supports *type polymorphism*.
   - Phileas has been used to automatically verify a large set of properties in
     Haskell's \texttt{base} package.
   _Future work_:
   - Will be integrated as part of the CDPOR project.
   - Generate verifiable proofs.
** \texttt{ }
    \begin{center} {\Huge
    Thank you!
    } \end{center}
